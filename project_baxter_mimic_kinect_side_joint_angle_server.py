import sys
import traceback
import code
import os

import copy
import math
import mathutils

import socket
import pickle
import threading

from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime


from math import radians

import ctypes
import pygame

if sys.hexversion >= 0x03000000:
    import _thread as thread
else:
    import thread

# colors for drawing different bodies 
SKELETON_COLORS = [pygame.color.THECOLORS["red"],
                   pygame.color.THECOLORS["blue"],
                   pygame.color.THECOLORS["green"],
                   pygame.color.THECOLORS["orange"],
                   pygame.color.THECOLORS["purple"],
                   pygame.color.THECOLORS["yellow"],
                   pygame.color.THECOLORS["violet"]]


counter=1

class BodyGameRuntime(object):
    def __init__(self):
        pygame.init()

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Set the width and height of the screen [width, height]
        self._infoObject = pygame.display.Info()
        self._screen = pygame.display.set_mode((self._infoObject.current_w >> 1, self._infoObject.current_h >> 1),
                                               pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE, 32)

        pygame.display.set_caption("Kinect for Windows v2 Body Game")

        # Loop until the user clicks the close button.
        self._done = False

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Kinect runtime object, we want only color and body frames 
        self._kinect = PyKinectRuntime.PyKinectRuntime(
            PyKinectV2.FrameSourceTypes_Color | PyKinectV2.FrameSourceTypes_Body)

        # back buffer surface for getting Kinect color frames, 32bit color, width and height equal to the Kinect color frame size
        self._frame_surface = pygame.Surface((self._kinect.color_frame_desc.Width, self._kinect.color_frame_desc.Height), 0, 32)

        # here we will store skeleton data 
        self._bodies = None

         # Create a TCP/IP socket
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self._socket.bind(("", 10000))

        self._clients=[]

        self._client_listener=threading.Thread(target=self.wait_for_clients,args=())
        """@type self._client_listener: threading.Thread"""
        self._client_listener.start()

    def wait_for_clients(self):
        self._socket.listen(1)
        while not self._done:
            clientsocket,addr = self._socket.accept()
            self._clients.append((clientsocket,addr))

        for (each_client,addr) in self._clients:
            each_client.close()


    def draw_body_bone(self, joints, jointPoints, color, joint0, joint1):

        if joints[joint0].TrackingState is PyKinectV2.TrackingState_NotTracked or \
            joints[joint1].TrackingState is PyKinectV2.TrackingState_NotTracked:
                return

        joint0State = joints[joint0].TrackingState;
        joint1State = joints[joint1].TrackingState;

        # both joints are not tracked
        if (joint0State == PyKinectV2.TrackingState_NotTracked) or (joint1State == PyKinectV2.TrackingState_NotTracked):
            return

        # both joints are not *really* tracked
        if (joint0State == PyKinectV2.TrackingState_Inferred) and (joint1State == PyKinectV2.TrackingState_Inferred):
            return

        # ok, at least one is good 
        start = (jointPoints[joint0].x, jointPoints[joint0].y)
        end = (jointPoints[joint1].x, jointPoints[joint1].y)

        try:
            pygame.draw.line(self._frame_surface, color, start, end, 4)
        except:  # need to catch it due to possible invalid positions (with inf)
            pass

    def draw_body(self, joints, jointPoints, color):
        # Torso
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_Head, PyKinectV2.JointType_Neck);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_Neck, PyKinectV2.JointType_SpineShoulder);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder,
                            PyKinectV2.JointType_SpineMid);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineMid, PyKinectV2.JointType_SpineBase);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder,
                            PyKinectV2.JointType_ShoulderRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder,
                            PyKinectV2.JointType_ShoulderLeft);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipLeft);

        # Right Arm    
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ShoulderRight,
                            PyKinectV2.JointType_ElbowRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ElbowRight,
                            PyKinectV2.JointType_WristRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristRight,
                            PyKinectV2.JointType_HandRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HandRight,
                            PyKinectV2.JointType_HandTipRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristRight,
                            PyKinectV2.JointType_ThumbRight);

        # Left Arm
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ShoulderLeft,
                            PyKinectV2.JointType_ElbowLeft);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ElbowLeft, PyKinectV2.JointType_WristLeft);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_HandLeft);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HandLeft,
                            PyKinectV2.JointType_HandTipLeft);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_ThumbLeft);

        # Right Leg
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HipRight, PyKinectV2.JointType_KneeRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_KneeRight,
                            PyKinectV2.JointType_AnkleRight);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_AnkleRight,
                            PyKinectV2.JointType_FootRight);

        # Left Leg
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HipLeft, PyKinectV2.JointType_KneeLeft);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_KneeLeft, PyKinectV2.JointType_AnkleLeft);
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_AnkleLeft, PyKinectV2.JointType_FootLeft);

    def draw_color_frame(self, frame, target_surface):
        target_surface.lock()
        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame.ctypes.data, frame.size)
        del address
        target_surface.unlock()


    def paint_frame(self,joint_type,joint_camera_position,joint_camera_rotation,joint_color_position):
        '''
            @type joint_type: int
            @type joint_camera_position: PyKinectV2._CameraSpacePoint
            @type joint_camera_rotation: mathutils.Quaternion
            @type joint_color_position: PyKinectV2._ColorSpacePoint
        '''
        div_val=15

        rot_matrix=joint_camera_rotation.to_matrix()

        frame_endpoints=[]
        for i in range(0,3):
            cam_space_coordinate=PyKinectV2._CameraSpacePoint()
            cam_space_coordinate.x=rot_matrix[0][i]/div_val+joint_camera_position.x
            cam_space_coordinate.y=rot_matrix[1][i]/div_val+joint_camera_position.y
            cam_space_coordinate.z=rot_matrix[2][i]/div_val+joint_camera_position.z
            col_space_coordinate=self._kinect._mapper.MapCameraPointToColorSpace(cam_space_coordinate)
            frame_endpoints.append(col_space_coordinate)

        frame_colors=[pygame.color.THECOLORS["red"],pygame.color.THECOLORS["blue"],pygame.color.THECOLORS["green"]]

        for i in range(0,3):
            start=(joint_color_position.x,joint_color_position.y)
            end=(frame_endpoints[i].x,frame_endpoints[i].y)
            pygame.draw.line(self._frame_surface, frame_colors[i], start, end, 5)

    #
    # def write_object(self,data):
    #     f = _socket.makefile('wb', buffer_size )
    #     pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
    #     f.close()

    def extract_and_print_joint_angles(self, jointOrientations,joints,jointPoints):
        '''
            @type joints: list[pykinect2.PyKinectV2._Joint]
            @type jointOrientations: list[pykinect2.PyKinectV2._JointOrientation]
        '''

        joint_list=[JointType_SpineBase,JointType_SpineMid,JointType_SpineShoulder, #0
                    JointType_Neck,JointType_Head, #3
                    JointType_ShoulderLeft,JointType_ElbowLeft,JointType_WristLeft,JointType_HandLeft, #5
                    JointType_ShoulderRight,JointType_ElbowRight,JointType_WristRight,JointType_HandRight, #9
                    JointType_HipLeft,JointType_KneeLeft,JointType_AnkleLeft,JointType_FootLeft, #13
                    JointType_HipRight,JointType_KneeRight,JointType_AnkleRight,JointType_FootRight, #17
                    JointType_HandTipLeft,JointType_ThumbLeft, #21
                    JointType_HandTipRight,JointType_ThumbRight] #23

        joint_rotation_array=[mathutils.Quaternion((jointOrientations[frame].Orientation.w,
                                  jointOrientations[frame].Orientation.x,
                                  jointOrientations[frame].Orientation.y,
                                  jointOrientations[frame].Orientation.z)) for frame in range(0,JointType_Count)]
        ''' @type joint_rotation_array: list[mathutils.Quaternion] '''



        #orient reference frame
        elbow_normal_rotation=joint_rotation_array[JointType_SpineShoulder]*mathutils.Quaternion((1,0,0),-math.pi/2)*mathutils.Quaternion((0,0,1),-math.pi/2)
        ''' @type elbow_normal_rotation: mathutils.Quaternion '''
        joint_rotation_array[JointType_SpineShoulder]=elbow_normal_rotation

        #-------------------------------------------------------
        ##LEFT ARM
        joint_rotation_array[JointType_ShoulderLeft]=joint_rotation_array[JointType_ElbowLeft]*mathutils.Quaternion((1,0,0),math.pi/2)*mathutils.Quaternion((0,1,0),math.pi/2)
        elbow_left_relative_to_normal_rotation=elbow_normal_rotation.inverted()*joint_rotation_array[JointType_ShoulderLeft]
        ''' @type elbow_left_relative_to_normal_rotation: mathutils.Quaternion '''
        elbow_rot_matrix=elbow_left_relative_to_normal_rotation.to_matrix()

        ###INVERSE KINEMATICS
        left_S0=math.atan2(elbow_rot_matrix[1][0],elbow_rot_matrix[0][0])   #Z
        left_E0=math.atan2(elbow_rot_matrix[2][1],elbow_rot_matrix[2][2])   #X
        try:
            left_S1=math.atan2(-elbow_rot_matrix[2][0],elbow_rot_matrix[1][0]/math.sin(left_S0)) #Y
        except ZeroDivisionError:
            left_S1=0

        ##LEFT FOREARM
        wrist_normal_rotation=joint_rotation_array[JointType_ShoulderLeft]
        joint_rotation_array[JointType_ElbowLeft]=joint_rotation_array[JointType_WristLeft]*mathutils.Quaternion((1,0,0),math.pi/2)*mathutils.Quaternion((0,1,0),math.pi/2)
        wrist_left_relative_to_normal_rotation=wrist_normal_rotation.inverted()*joint_rotation_array[JointType_ElbowLeft]
        ''' @type elbow_left_relative_to_normal_rotation: mathutils.Quaternion '''
        wrist_rot_matrix=wrist_left_relative_to_normal_rotation.to_matrix()

        left_E1=-math.atan2(-wrist_rot_matrix[2][0],wrist_rot_matrix[0][0]) #Y
        left_W0=math.atan2(-wrist_rot_matrix[1][2],wrist_rot_matrix[1][1]) #X


        #-------------------------------------------------------
        ##RIGHT ARM
        joint_rotation_array[JointType_ShoulderRight]=joint_rotation_array[JointType_ElbowRight]*mathutils.Quaternion((1,0,0),-math.pi/2)*mathutils.Quaternion((0,1,0),-math.pi/2)
        elbow_right_relative_to_normal_rotation=elbow_normal_rotation.inverted()*joint_rotation_array[JointType_ShoulderRight]
        ''' @type elbow_right_relative_to_normal_rotation: mathutils.Quaternion '''
        elbow_rot_matrix=elbow_right_relative_to_normal_rotation.to_matrix()

        ###INVERSE KINEMATICS
        right_S0=math.atan2(elbow_rot_matrix[1][0],elbow_rot_matrix[0][0])
        right_E0=math.atan2(elbow_rot_matrix[2][1],elbow_rot_matrix[2][2])
        try:
            right_S1=math.atan2(-elbow_rot_matrix[2][0],elbow_rot_matrix[1][0]/math.sin(right_S0))
        except ZeroDivisionError:
            right_S1=0

        #RIGHT FOREARM
        wrist_normal_rotation=joint_rotation_array[JointType_ShoulderRight]
        joint_rotation_array[JointType_ElbowRight]=joint_rotation_array[JointType_WristRight]*mathutils.Quaternion((1,0,0),-math.pi/2)*mathutils.Quaternion((0,1,0),-math.pi/2)
        wrist_right_relative_to_normal_rotation=wrist_normal_rotation.inverted()*joint_rotation_array[JointType_ElbowRight]
        ''' @type elbow_left_relative_to_normal_rotation: mathutils.Quaternion '''
        wrist_rot_matrix=wrist_right_relative_to_normal_rotation.to_matrix()

        right_E1=-math.atan2(-wrist_rot_matrix[2][0],wrist_rot_matrix[0][0]) #Y
        right_W0=math.atan2(-wrist_rot_matrix[1][2],wrist_rot_matrix[1][1]) #X

        joint_print_list=[JointType_SpineShoulder,
            JointType_ShoulderLeft,JointType_ElbowLeft,
            JointType_ShoulderRight,JointType_ElbowRight]

        try:
            for joint in joint_print_list:#[JointType_SpineShoulder,JointType_ShoulderLeft,JointType_ShoulderRight]:
                self.paint_frame(joint,joints[joint].Position,joint_rotation_array[joint],jointPoints[joint])
        except:
            pass


        angles={'right_s0':right_S0,
                'right_s1':right_S1,
                'right_e0':right_E0,
                'right_e1':right_E1,
                'right_w0':right_W0,
                'right_w1':0,
                'right_w2':0,
                'left_s0':left_S0,
                'left_s1':left_S1,
                'left_e0':left_E0,
                'left_e1':left_E1,
                'left_w0':left_W0,
                'left_w1':0,
                'left_w2':0}

        return angles

    def run(self):

        # -------- Main Program Loop -----------
        while not self._done:
            # --- Main event loop
            for event in pygame.event.get():  # User did something
                if event.type == pygame.QUIT:  # If user clicked close
                    self._done = True  # Flag that we are done so we exit this loop

                elif event.type == pygame.VIDEORESIZE:  # window resized
                    self._screen = pygame.display.set_mode(event.dict['size'],
                                                           pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE, 32)

            # --- Game logic should go here

            # --- Getting frames and drawing  
            # --- Woohoo! We've got a color frame! Let's fill out back buffer surface with frame's data 
            if self._kinect.has_new_color_frame():
                frame = self._kinect.get_last_color_frame()
                self.draw_color_frame(frame, self._frame_surface)
                frame = None

            # --- Cool! We have a body frame, so can get skeletons
            if self._kinect.has_new_body_frame():
                self._bodies = self._kinect.get_last_body_frame()


            # if self._kinect.has_new_depth_frame():
            #     self._depth=self._kinect.get_last_depth_frame()

            # --- draw skeletons to _frame_surface
            if self._bodies is not None:

                closest_z=99999
                closest_x=99999
                closest_y=99999
                closest_body=None
                try:
                    for i in range(0,self._kinect.max_body_count):
                        if not self._bodies.bodies[i].is_tracked:
                            continue
                        this_body_z=self._bodies.bodies[i].joints[JointType_SpineShoulder].Position.z
                        if this_body_z < closest_z:
                            closest_z=this_body_z
                            closest_x=self._bodies.bodies[i].joints[JointType_SpineShoulder].Position.x
                            closest_y=self._bodies.bodies[i].joints[JointType_SpineShoulder].Position.y
                            closest_body=self._bodies.bodies[i]
                except:
                    pass



                if (closest_x < 0.24 and closest_x > -1.0)  and (closest_z < 2.1 and closest_z > 1.0):
                    print(str(closest_x)+" "+str(closest_y)+" "+str(closest_z))
                    if closest_body is not None:
                        body = closest_body

                        if not body.is_tracked:
                            continue

                        joints = body.joints
                        # convert joint coordinates to color space
                        joint_points = self._kinect.body_joints_to_color_space(joints)
                        self.draw_body(joints, joint_points, SKELETON_COLORS[i])

                        jointOrientations = body.joint_orientations
                        joint_angles=self.extract_and_print_joint_angles(jointOrientations,joints,joint_points)

                        #----------------------------------

                        global counter

                        if counter%10 is 0:
                            # #print("tick "+str(counter))
                            # pj=joint_angles.copy()
                            # for i in pj:
                            #     pj[i]=pj[i]*180/math.pi
                            # print(str(pj['left_e1'])+" "+str(pj['left_w0']))

                            try:
                                for (each_client,addr) in self._clients:
                                    f = each_client.makefile('wb', 200 )
                                    pickle.dump(joint_angles, f, protocol=2)
                                    f.close()
                            except:
                                for (each_client,addr) in self._clients:
                                    each_client.shutdown(socket.SHUT_RDWR)
                                    each_client.close()
                                self._clients.clear()
                        counter=counter+1

            # --- copy back buffer surface pixels to the screen, resize it if needed and keep aspect ratio
            # --- (screen size may be different from Kinect's color frame size) 
            h_to_w = float(self._frame_surface.get_height()) / self._frame_surface.get_width()
            target_height = int(h_to_w * self._screen.get_width())
            surface_to_draw = pygame.transform.scale(self._frame_surface, (self._screen.get_width(), target_height));
            surface_to_draw = pygame.transform.flip(surface_to_draw, True,False);
            self._screen.blit(surface_to_draw, (0, 0))
            surface_to_draw = None

            pygame.display.update()

            # --- Go ahead and update the screen with what we've drawn.
            pygame.display.flip()

            # --- Limit to 60 frames per second
            self._clock.tick(60)

        # Close our Kinect sensor, close the window and quit.
        self._kinect.close()
        self._client_listener.join()
        self._socket.close()
        pygame.quit()


if __name__ == '__main__':
    try:
        __main__ = "Baxter Mimic Game, Joint Angle Server"
        game = BodyGameRuntime()
        game.run()
    except:
        type, value, tb = sys.exc_info()
        traceback.print_exc()
        last_frame = lambda tb=tb: last_frame(tb.tb_next) if tb.tb_next else tb
        frame = last_frame().tb_frame
        ns = dict(frame.f_globals)
        ns.update(frame.f_locals)
        code.interact(local=ns)
