#!/usr/bin/env python

__author__ = 'ifyalciner'

import sys
import traceback
import code
import os
import argparse
import random
import socket
import pickle
import time
import threading
import mutex
import copy

import math
import numpy
import pyrr

import rospy

import baxter_interface

from baxter_interface import CHECK_VERSION

from std_msgs.msg import (
    UInt16,
)


counter=1

server_ip='192.168.0.101'

class Teleoperator(object):

    def __init__(self):
        self._done = False

        self._pub_rate = rospy.Publisher('robot/joint_state_publish_rate',
                                         UInt16, queue_size=10)

        self._rate = 500.0  # Hz

        class my_baxter:pass
        self._baxter=my_baxter()

        self._baxter._head = baxter_interface.Head()
        self._baxter._right_limb=baxter_interface.Limb('right')
        self._baxter._left_limb=baxter_interface.Limb('left')

        self._baxter._left_limb.set_joint_position_speed(1.0)
        self._baxter._right_limb.set_joint_position_speed(1.0)

        # verify robot is enabled
        print("Getting robot state... ")
        self._baxter._robot_state = baxter_interface.RobotEnable(CHECK_VERSION)
        self._baxter._init_state = self._baxter._robot_state.state().enabled

        print("Enabling robot... ")
        self._baxter._robot_state.enable()
        print("Running. Ctrl-c to quit")

        self._pub_rate.publish(self._rate)

        self._angular_speed_data=[]
        self._angular_speed_data_mutex=mutex.mutex()

        self._angle_data_size_limit=3
        self._angle_data_list=[]

        self._angle_data_mutex=mutex.mutex()
        self._angle_data_mean=[]
        self._angle_data=[]

        self._angle_listener_thread=threading.Thread(target=self.angle_listener, args=())

        self.set_neutral()


    def _reset_control_modes(self):
        rate = rospy.Rate(self._rate)
        for _ in xrange(100):
            if rospy.is_shutdown():
                return False
            self._baxter._left_limb.exit_control_mode()
            self._baxter._right_limb.exit_control_mode()
            self._pub_rate.publish(100)  # 100Hz default joint state rate
            rate.sleep()
        return True

    def clean_shutdown(self):
        """
        Exits example cleanly by moving head to neutral position and
        maintaining start state
        """
        print("\nExiting example...")
        self.set_neutral()
        self._reset_control_modes()
        if self._done:
            self.set_neutral()
        if not self._baxter._init_state and self._baxter._robot_state.state().enabled:
            print("Disabling robot...")
            self._baxter._robot_state.disable()

    def set_neutral(self):
        """
        Sets the head back into a neutral pose
        """

        neutral_angles={'right_s0':0,
                        'right_s1':0,
                        'right_e0':0,
                        'right_e1':0,
                        'right_w0':0,
                        'right_w1':0,
                        'right_w2':0,
                        'left_s0':0,
                        'left_s1':0,
                        'left_e0':0,
                        'left_e1':0,
                        'left_w0':0,
                        'left_w1':0,
                        'left_w2':0}

        self.set_baxter_joint_angles(neutral_angles)

    def set_baxter_joint_angles(self,new_angles):

        """
        Sets the head back into a neutral pose
        """
        self._baxter._head.set_pan(0.0)

        angles = self._baxter._right_limb.joint_angles()

        angles['right_s0']=new_angles['right_s0']+math.pi/4
        angles['right_s1']=new_angles['right_s1']
        angles['right_e0']=new_angles['right_e0']+math.pi
        angles['right_e1']=new_angles['right_e1']
        angles['right_w0']=new_angles['right_w0']
        angles['right_w1']=new_angles['right_w1']
        angles['right_w2']=new_angles['right_w2']

        self._baxter._right_limb.move_to_joint_positions(angles,timeout=0.5,threshold=0.1) # moving right arm to all 0 joint angles


        angles = self._baxter._left_limb.joint_angles()

        angles['left_s0']=new_angles['left_s0']-math.pi/4
        angles['left_s1']=new_angles['left_s1']
        angles['left_e0']=new_angles['left_e0']-math.pi
        angles['left_e1']=new_angles['left_e1']
        angles['left_w0']=new_angles['left_w0']
        angles['left_w1']=new_angles['left_w1']
        angles['left_w2']=new_angles['left_w2']

        self._baxter._left_limb.move_to_joint_positions(angles,timeout=0.5,threshold=0.1) # moving left arm to all 0 joint angles


    def angle_listener(self):
        '''@type stop_event: threading.Event'''

        while not rospy.is_shutdown():
            try:
                if self._socket is None:
                     # Create a TCP/IP socket
                    self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            except:
                self._socket=None
                time.sleep(1)
                continue

            try:
                print("trying to connect"+ str(server_ip))
                self._socket.connect((server_ip,10000))
                print("connected...")

                while not rospy.is_shutdown():
                    f = self._socket.makefile('rb', 200 )
                    data = pickle.load(f)
                    f.close()

                    self._angle_data_list.insert(0,data)

                    if len(self._angle_data_list) > self._angle_data_size_limit:
                        self._angle_data_list.pop()

                    new_mean={}
                    size=len(self._angle_data_list)
                    for elem in self._angle_data_list:
                        for joint_name in elem:
                            try:
                                new_mean[joint_name]=new_mean[joint_name]+(elem[joint_name]/size)
                            except KeyError:
                                new_mean[joint_name]=(elem[joint_name]/size)
                            except:
                                print "Unexpected error:", sys.exc_info()[0]
                                raise

                    if self._angle_data_mutex.testandset() is True:
                        self._angle_data_mean=new_mean
                        self._angle_data_mutex.unlock()

            except:
                self._angle_data_mutex.unlock()
                self._socket.close()
                self._socket=None
                time.sleep(2)
            # except:
            #     print "Unexpected error:", sys.exc_info()[0]
            #     raise


    def get_angular_speed(self,current_angle,desired_angle,domain):
        return_val=0
        if math.fabs(desired_angle-current_angle) > math.pi/30:
            return_val=(desired_angle-current_angle)*0.9#(desired_angle-current_angle)*(math.fabs((desired_angle-current_angle)/domain)**0.3)
        return return_val

    def run(self):

        self._angle_listener_thread.start()
        angle_counter=0
        old_angle=0
        while not rospy.is_shutdown():
            self._pub_rate.publish(self._rate)
            if self._angle_data_mutex.testandset() is True:
                new_angles=copy.copy(self._angle_data_mean)
                self._angle_data_mutex.unlock()

                if new_angles and (new_angles is not None):

                    if not old_angle or self.get_angular_speed(new_angles['right_s1'],old_angle,3.194) is 0:
                        angle_counter=angle_counter+1
                    else:
                        angle_counter=0

                    old_angle=new_angles['right_s1']

                    if angle_counter > 10000:
                        angles = self._baxter._right_limb.joint_angles()

                        right_joint_velocities={'right_s0':self.get_angular_speed(angles['right_s0'],0+math.pi/4,3.351),
                                                'right_s1':self.get_angular_speed(angles['right_s1'],0,3.194),
                                                'right_e0':self.get_angular_speed(angles['right_e0'],0+math.pi,6.056),
                                                'right_e1':self.get_angular_speed(angles['right_e1'],0,2.67),
                                                'right_w0':self.get_angular_speed(angles['right_w0'],0,6.117),
                                                'right_w1':self.get_angular_speed(angles['right_w1'],0,6.117),
                                                'right_w2':self.get_angular_speed(angles['right_w2'],0,6.117)}

                        self._baxter._right_limb.set_joint_velocities(right_joint_velocities)




                        angles = self._baxter._left_limb.joint_angles()
                        left_joint_velocities={ 'left_s0':self.get_angular_speed(angles['left_s0'],0-math.pi/4,3.351),
                                                'left_s1':self.get_angular_speed(angles['left_s1'],0,3.194),
                                                'left_e0':self.get_angular_speed(angles['left_e0'],0-math.pi,6.056),
                                                'left_e1':self.get_angular_speed(angles['left_e1'],0,2.67),
                                                'left_w0':self.get_angular_speed(angles['left_w0'],0,6.117),
                                                'left_w1':self.get_angular_speed(angles['left_w1'],0,6.117),
                                                'left_w2':self.get_angular_speed(angles['left_w2'],0,6.117)}

                        self._baxter._left_limb.set_joint_velocities(left_joint_velocities)

                    else:

                        angles = self._baxter._right_limb.joint_angles()

                        right_joint_velocities={'right_s0':self.get_angular_speed(angles['right_s0'],new_angles['right_s0']+math.pi/4,3.351),
                                                'right_s1':self.get_angular_speed(angles['right_s1'],new_angles['right_s1'],3.194),
                                                'right_e0':self.get_angular_speed(angles['right_e0'],new_angles['right_e0']+math.pi,6.056),
                                                'right_e1':self.get_angular_speed(angles['right_e1'],new_angles['right_e1'],2.67),
                                                'right_w0':self.get_angular_speed(angles['right_w0'],new_angles['right_w0'],6.117),
                                                'right_w1':self.get_angular_speed(angles['right_w1'],0,6.117),
                                                'right_w2':self.get_angular_speed(angles['right_w2'],0,6.117)}

                        self._baxter._right_limb.set_joint_velocities(right_joint_velocities)




                        angles = self._baxter._left_limb.joint_angles()
                        left_joint_velocities={ 'left_s0':self.get_angular_speed(angles['left_s0'],new_angles['left_s0']-math.pi/4,3.351),
                                                'left_s1':self.get_angular_speed(angles['left_s1'],new_angles['left_s1'],3.194),
                                                'left_e0':self.get_angular_speed(angles['left_e0'],new_angles['left_e0']-math.pi,6.056),
                                                'left_e1':self.get_angular_speed(angles['left_e1'],new_angles['left_e1'],2.67),
                                                'left_w0':self.get_angular_speed(angles['left_w0'],new_angles['left_w0'],6.117),
                                                'left_w1':self.get_angular_speed(angles['left_w1'],0,6.117),
                                                'left_w2':self.get_angular_speed(angles['left_w2'],0,6.117)}

                        self._baxter._left_limb.set_joint_velocities(left_joint_velocities)
                else:
                    angles = self._baxter._right_limb.joint_angles()

                    right_joint_velocities={'right_s0':self.get_angular_speed(angles['right_s0'],0+math.pi/4,3.351),
                                            'right_s1':self.get_angular_speed(angles['right_s1'],0,3.194),
                                            'right_e0':self.get_angular_speed(angles['right_e0'],0+math.pi,6.056),
                                            'right_e1':self.get_angular_speed(angles['right_e1'],0,2.67),
                                            'right_w0':self.get_angular_speed(angles['right_w0'],0,6.117),
                                            'right_w1':self.get_angular_speed(angles['right_w1'],0,6.117),
                                            'right_w2':self.get_angular_speed(angles['right_w2'],0,6.117)}

                    self._baxter._right_limb.set_joint_velocities(right_joint_velocities)




                    angles = self._baxter._left_limb.joint_angles()
                    left_joint_velocities={ 'left_s0':self.get_angular_speed(angles['left_s0'],0-math.pi/4,3.351),
                                            'left_s1':self.get_angular_speed(angles['left_s1'],0,3.194),
                                            'left_e0':self.get_angular_speed(angles['left_e0'],0-math.pi,6.056),
                                            'left_e1':self.get_angular_speed(angles['left_e1'],0,2.67),
                                            'left_w0':self.get_angular_speed(angles['left_w0'],0,6.117),
                                            'left_w1':self.get_angular_speed(angles['left_w1'],0,6.117),
                                            'left_w2':self.get_angular_speed(angles['left_w2'],0,6.117)}

                    self._baxter._left_limb.set_joint_velocities(left_joint_velocities)

        print('Killing Angle Listener')
        self._angle_listener_thread.join()

        rospy.signal_shutdown("Example finished.")


def main():
    rospy.init_node('baxter_teleoperation')
    to = Teleoperator()
    rospy.on_shutdown(to.clean_shutdown)
    #to.set_neutral()
    to.run()

if __name__ == '__main__':
    # __main__ = "Baxter Master"
    # main()

    try:
        __main__ = "Baxter Master"
        main()
    except:
        type, value, tb = sys.exc_info()
        traceback.print_exc()
        last_frame = lambda tb=tb: last_frame(tb.tb_next) if tb.tb_next else tb
        frame = last_frame().tb_frame
        ns = dict(frame.f_globals)
        ns.update(frame.f_locals)
        code.interact(local=ns)

